package ie.ucd.hellospringboot;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AuthenticationController {
    @GetMapping("/login")
    public String login(@RequestParam(name="username") String username,
                      @RequestParam(name="password") String password, Model model) {

        if (username.equals("rem") && password.equals("sausage")) {
            model.addAttribute("username", username);
            return "success";
        }
        return "failure";
    }
}